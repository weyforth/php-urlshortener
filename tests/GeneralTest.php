<?php

require(__DIR__.'/../src/Weyforth/Shortener/Shortener.php');

use \Weyforth\Shortener\Shortener;

class GeneralTest extends PHPUnit_Framework_Testcase
{

    public function testIntegerConvert1(){
        $converted = Shortener::shorten(1);
        $this->assertEquals('b', $converted);
        $this->assertInternalType('string', $converted);
    }

    public function testIntegerBackB(){
        $back = Shortener::back('b');
        $this->assertEquals(1, $back);
        $this->assertInternalType('int', $back);
    }

    public function testIntegerConvert23039(){
        $converted = Shortener::shorten(23039);
        $this->assertEquals('glQ', $converted);
        $this->assertInternalType('string', $converted);
    }

    public function testIntegerBackGLQ(){
        $back = Shortener::back('glQ');
        $this->assertEquals(23039, $back);
        $this->assertInternalType('int', $back);
    }

    public function testIntegerConvert1String(){
        $converted = Shortener::shorten('1');
        $this->assertEquals('b', $converted);
        $this->assertInternalType('string', $converted);
    }

    public function testIntegerConvertString(){
        $converted = Shortener::shorten('testing');
        $this->assertEquals('a', $converted);
        $this->assertInternalType('string', $converted);
    }

    public function testIntegerBackString123(){
        $back = Shortener::back('123');
        $backInt = Shortener::back(123);
        $this->assertEquals($back, $backInt);
    }

}
