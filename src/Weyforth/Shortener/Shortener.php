<?php
/**
 * Utility to assist in creation of reversable shortened strings from integers.
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\Shortener;

class Shortener
{

    /**
     * Integers to convert from.
     *
     * @var string
     */
    public static $numbers = '0123456789';

    /**
     * Integers and characters to convert to.
     *
     * @var string
     */
    public static $converted = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';


    /**
     * Convert from one alphabet to the other.
     *
     * @param string $src         Source string.
     * @param string $srcAlphabet Source alphabet.
     * @param string $dstAlphabet Destination alphabet.
     *
     * @return string
     */
    protected static function convert($src, $srcAlphabet, $dstAlphabet)
    {
        $srcBase = strlen($srcAlphabet);
        $dstBase = strlen($dstAlphabet);

        $wet = $src;
        $val = 0;
        $mlt = 1;

        while ($l = strlen($wet)) {
            $digit = $wet[($l - 1)];
            $val  += ($mlt * strpos($srcAlphabet, $digit));
            $wet   = substr($wet, 0, ($l - 1));
            $mlt  *= $srcBase;
        }

        $wet = $val;
        $dst = '';

        while ($wet >= $dstBase) {
            $digitVal = ($wet % $dstBase);
            $digit    = $dstAlphabet[intval($digitVal)];
            $dst      = $digit.$dst;
            $wet     /= $dstBase;
        }

        $digit = $dstAlphabet[intval($wet)];
        $dst   = $digit.$dst;

        return $dst;
    }


    /**
     * Convert integer to shortened string.
     *
     * @param integer $id ID to shorten.
     *
     * @return string
     */
    public static function shorten($id)
    {
        return self::convert(
            (string) $id,
            self::$numbers,
            self::$converted
        );
    }


    /**
     * Convert shortened string back to integer.
     *
     * @param string $converted String to revert.
     *
     * @return integer
     */
    public static function back($converted)
    {
        return intval(
            self::convert(
                (string) $converted,
                self::$converted,
                self::$numbers
            )
        );
    }


}
